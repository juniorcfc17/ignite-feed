import { Header } from "./components/Header";
import { Post } from "./components/Post";
import { Sidebar } from "./components/Sidebar";

import './global.css'
import styles from './App.module.css'

export default function App() {
  const posts = [
    {
      id: 1,
      author: {
        avatarUrl: 'https://github.com/adilsonjrs.png',
        name: 'Adilson Jr',
        role: 'Dev Fullstack',
      },
      content: [
        {type: 'paragraph', content: 'Fala galera.'},
        {type: 'paragraph', content: 'Acabei de subir mais um projeto no meu portifólio.'},
        {type: 'link', content: 'jane.design/doctorcare'}      
      ],
      publishedAt: new Date('2023-02-02 20:00:00'),
    },
    {
      id: 2,
      author: {
        avatarUrl: 'https://github.com/diego3g.png',
        name: 'Diego Fernandes',
        role: 'CTO @Rocketseat'
      },
      content: [
        {type: 'paragraph', content: 'Fala galera Acabei de subir mais um projeto no meu portifólio.'},
        {type: 'link', content: 'jane.design/doctorcare'}      
      ],
      publishedAt: new Date('2023-02-02 20:00:00'),
    },
    {
      id: 3,
      author: {
        avatarUrl: 'https://github.com/maykbrito.png',
        name: 'Mayk Brito',
        role: 'Dev Fullstack',
      },
      content: [
        {type: 'paragraph', content: 'Fala galera Acabei de subir mais um projeto no meu portifólio.'},
        {type: 'link', content: 'jane.design/doctorcare'}      
      ],
      publishedAt: new Date('2023-02-02 20:00:00'),
    }
  ]

  return (
    <>
      <Header />

      <div className={styles.wrapper}>
        <Sidebar />
        <main>
          {posts.map(post => {
            return (
              <Post
                key={post.id}
                author={post.author}
                content={post.content}
                publishedAt={post.publishedAt}
              />
            )
          })}
        </main>
      </div>
    </>
  )
}

